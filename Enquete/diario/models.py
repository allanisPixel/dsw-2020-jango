from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Usuario(models.Model):
    user = models.OneToOneField(
        User, on_delete = models.CASCADE,
        null=True,
        default=None,
        verbose_name = 'Usuário',
    )
    nick = models.CharField(
        'Apelido',
        max_length = 20,
    )
    biografia = models.TextField(
        max_length = 2000
    )
    data_nascimento = models.DateField(
        'Data de nascimento', null=True, default=None
    )
    def __str__(self):
        return self.nick

    #usuario tem login e senha e um diario
class Diario(models.Model):
    #diario tem entradas
    #tem que ter chave pra usuario e entradas
    nome_diario = models.CharField(
        'Nome do Diario',
        max_length = 20,
        blank = True
    )
    dono = models.OneToOneField(
        User, on_delete=models.CASCADE, null=True, default=None
    )
    def __str__(self):
        return self.nome_diario
    def todos_os_diarios(self):
        return self.nome_diario


class Entrada(models.Model):
    diario = models.ForeignKey(
        Diario, on_delete=models.CASCADE, null=True, default=None
    )

    data = models.DateTimeField(
        auto_now_add = True
    )
    titulo = models.CharField(
        'Título da Entrada',
        max_length = 120,
        blank = True,
        null = True,
    )
    texto = models.TextField(
        max_length = 5000
    )
    imagem = models.ImageField(
        upload_to = 'entrada/media',
        null=True, default=None,
    )
    def __str__(self):
        return self.titulo


