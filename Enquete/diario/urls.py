from django.urls import path
from . import views

app_name = 'diario'
urlpatterns = [
    path('',
        views.IndexView.as_view(), name='index'
    ),
    path('<int:pk>',
        views.DiarioView.as_view(), name='diario'
    ),
    path('entrada/<int:pk>',
        views.EntradaView.as_view(), name='entrada'
    ),
    path('perfio/<int:pk>',
        views.PerfioView.as_view(), name='perfio'
    ),
]