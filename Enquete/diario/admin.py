from django.contrib import admin
from .models import Usuario, Diario, Entrada

# Register your models here.
admin.site.register(Usuario)
admin.site.register(Diario)
admin.site.register(Entrada)