from django.views import generic
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .models import Usuario, Diario, Entrada
##from django.contrib.auth.forms import authenticate, login, logout


# Create your views here.
# por enquanto exibe os usuarios
@method_decorator(login_required, name='dispatch')
class IndexView(generic.ListView):
    template_name = 'diario/index.html'
    model = Diario
    context_object_name = 'os_diarios'

#Exibir o que tem dentro do diario(as entradas)
@method_decorator(login_required, name='dispatch')
class DiarioView(generic.DetailView):
    template_name = 'diario/diario.html'
    model = Diario

#Exibir a entrada
@method_decorator(login_required, name='dispatch')
class EntradaView(generic.DetailView):
    template_name = 'diario/entrada.html'
    model = Entrada

@method_decorator(login_required, name='dispatch')
class PerfioView(generic.DetailView):
    template_name = 'diario/perfio.html'
    model = Usuario





