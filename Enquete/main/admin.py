from django.contrib import admin
from .models import Pergunta, Opcao, Classificacao

class OpcaoInline(admin.TabularInline):
    model = Opcao
    extra = 1

class PerguntaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields':['texto']}),
        ('Informações de data',{'fields':['data_publicacao']}),
        ('Informações de classificacao',{'fields':['classificacao']}),
    ]
    inlines = [OpcaoInline]
    list_display = ('texto', 'id', 'data_publicacao', 'publicada_recentemente')
    list_filter = ['data_publicacao']
    search_fields = ['texto']

# Register your models here.
admin.site.register(Pergunta, PerguntaAdmin)
admin.site.register(Classificacao)
#admin.site.register(Opcao)