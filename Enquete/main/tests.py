import datetime
from django.utils import timezone
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
from .models import Pergunta

# Create your tests here.
#=============================Testes Models=====================================
class PerguntaTeste(TestCase):
    def test_pergunta_publicada_recentemente_com_pergunta_no_futuro(self):#self?
        """
        são 2 boas ideias: 1: usar nome de metodo, mesmo que grande,
        auto esplicativo; 2: Escrever um comentario como esse dentro do metodo
        pois elimina a necessidade de documentar os testes para terceiros testadores.

        O metodo publicada_recentemente precisa retornar 'False' quando se trata
        de perguntas com data de publicação 'no futuro'
        """
        data_futura = timezone.now() + datetime.timedelta(seconds=1)
        pergunta_futura = Pergunta(data_publicacao = data_futura)
        self.assertIs(pergunta_futura.publicada_recentemente(), False)

    def test_pergunta_publicada_recentemente_com_data_anterior_a_24h_no_passado(self):
        """O metodo publicada_recentemente DEVE retornar FALSE quando se tratar
           de uma data de uma data de publicação anterior a 24hs no passado.
        """
        data_passada = timezone.now() - datetime.timedelta(days=1, seconds=1)
        pergunta_passada = Pergunta(data_publicacao = data_passada)
        self.assertIs(pergunta_passada.publicada_recentemente(), False)

    def test_pergunta_publicada_recentemente_com_data_nas_ultimas_24h(self):
        """o metodo publicada recentemente Deve retornar True quando se tratar de
           uma data de publicação dentro das ultimas 24H.
        """
        data = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        pergunta = Pergunta(data_publicacao = data)
        self.assertIs(pergunta.publicada_recentemente(), True)
#=============================================================================
#--------------------------------metodo auxiliar------------------------------
def criar_pergunta(texto, dias):
    """Função para a criação de uma pergunta com texto e data de publicação,
       a qual pode ser positiva ou negativa, contados a partir do dia corrente
    """
    data = timezone.now() + datetime.timedelta(days=dias)
    return Pergunta.objects.create(texto=texto, data_publicacao=data)
#-----------------------------------------------------------------------------
#==============================Teste de View==============================

class IndexViewTeste(TestCase):
    def test_sem_perguntas_cadastradas(self):
        """Test da IndexView quando não houver perguntas cadastradas,deverá ser
           Exibida uma mensagem especifica.
        """
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta,"Não há enquetes cadastradas até o momento!")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'], [])

    def test_com_pergunta_no_passado(self):
        """
        Test da IndexView exibindo normalmente pergunta no passado.
        """
        criar_pergunta(texto='Pergunta no passado', dias=-30)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        #self.assertContains(resposta,"Não há enquetes cadastradas até o momento!")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'], ['<Pergunta: Pergunta no passado>'])

    def test_com_pergunta_no_futuro(self):
        """Apenas questões com data de publicação no passado são exibidas
        as com data no futuro NÃO DEVEM ser exibidas.
        """
        criar_pergunta(texto="pergunta no futuro", dias=1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta,"Não há enquetes cadastradas até o momento!")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'], [])

    def test_pergunta_no_passado_e_no_futuro(self):
        """
        Perguntas com data de publicação no passado são exibidas e com data de
        publicação no futuro são omitidas.
        """
        criar_pergunta(texto="pergunta no passado", dias=-1)
        criar_pergunta(texto="pergunta no futuro", dias=1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta,"pergunta no passado")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'],
        ['<Pergunta: pergunta no passado>'])

    def test_duas_perguntas_no_passado(self):
        """
        Exibe normalmente mais de uma Pergunta com data de publicação no passado.
        """
        criar_pergunta(texto="pergunta no passado 1", dias=-1)
        criar_pergunta(texto="pergunta no passado 2", dias=-5)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta,"pergunta no passado")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'],
        ['<Pergunta: pergunta no passado 1>',
        '<Pergunta: pergunta no passado 2>'])


class DetalhesViewTeste(TestCase):
    def test_pergunta_no_futuro(self):
        """
        Deverá retornar um erro 404 ao indicar uma pergunta com data no futuro.
        """
        pergunta_futura = criar_pergunta(texto="pergunta no futuro", dias=5)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta_futura.id,])
        )
        self.assertEqual(resposta.status_code, 404)

    def test_pergunta_no_passado(self):
        """
        Deverá exibir normalmente uma pergunta com data no passado.
        """
        pergunta_passada = criar_pergunta(texto="pergunta no passado", dias=-1)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta_passada.id,])
        )
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta,pergunta_passada.texto)
#===============================================================================















