from django.db import models
from django.utils import timezone
import datetime

# Create your models here.
class Classificacao(models.Model):
    titulo = models.CharField(max_length=30)
    descricao = models.CharField(max_length=200)
    data_criacao = models.DateTimeField('Data de criação')
    def __str__(self):
        return self.titulo

#classe pergunta(que erda de models.Model)
class Pergunta(models.Model):
    texto = models.CharField(max_length=200)
    data_publicacao = models.DateTimeField('Data de publicação')
    classificacao = models.ForeignKey(Classificacao, on_delete=models.CASCADE, null=True, default=None )
    def __str__(self):
        #padrão receber self
        return self.texto
    def publicada_recentemente(self):
        agora = timezone.now()
        return agora-datetime.timedelta(days=1) <= self.data_publicacao <= agora
        #    (passado)                          (tempo da pergunta)  (não no futuro)

    publicada_recentemente.admin_order_field = 'data_publicacao'
    publicada_recentemente.boolean = True
    publicada_recentemente.short_description = 'Últimas 24h?'

class Opcao(models.Model):
    # Qual a naturesa desse models.XXXXXX? é um tipo? é uma erança?
    texto = models.CharField(max_length = 200)
    votos = models.IntegerField(default = 0)
    #fazendo ligação entre 2 classes e o banco (mapear relacionamento):
    pergunta = models.ForeignKey(Pergunta,on_delete=models.CASCADE)
    def __str__(self):
        return self.texto


