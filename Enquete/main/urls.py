from django. urls import path
from . import views

app_name = 'main'
urlpatterns = [
    path('',
        views.IndexView.as_view(), name='index'
    ),
    path('<int:pk>', views.DetalhesView.as_view(), name='detalhes'
    ),
    path('<int:pk>/resultado', views.ResultadoView.as_view(), name='resultado'
    ),
    path('<int:id_enquete>/voltacao', views.votacao, name='votacao'
    ),
    path('classificacao/<int:pk>', views.ClassificacaoView.as_view(), name='classificacao'
    ),
]